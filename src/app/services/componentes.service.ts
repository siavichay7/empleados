import { Injectable, ChangeDetectorRef } from '@angular/core';
import Swal, { SweetAlertIcon } from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ComponentesService {
  constructor() { }

  alerta(icono: any, texto: string) {
    return Swal.fire({
      icon: icono,
      text: texto,
    });
  }

  alertaButtons(titulo: string, texto: string = "", icon: SweetAlertIcon = 'question') {
    return Swal.fire({
      title: titulo,
      text: texto,
      icon: 'question',
      showDenyButton: true,
      showCancelButton: false,
      denyButtonText: `No`,
      confirmButtonText: `Si`,
    })
  }

}
