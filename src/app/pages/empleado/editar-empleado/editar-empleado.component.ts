import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ComponentesService } from 'src/app/services/componentes.service';
import { Empleado } from '../../model/empleado.interface';

@Component({
  selector: 'app-editar-empleado',
  templateUrl: './editar-empleado.component.html',
  styleUrls: ['./editar-empleado.component.scss']
})
export class EditarEmpleadoComponent implements OnInit {

  empleadoForm!: FormGroup;
  departamentos: any[] = ["Administración", "Tienda", "Contabilidad"];
  empleados: any[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) public empleado: Empleado,
    private fb: FormBuilder,
    private componentService: ComponentesService,
    private dialogRef: MatDialogRef<EditarEmpleadoComponent>,

  ) { }

  ngOnInit(): void {
    this.empleadoForm = this.fb.group({
      nombres: [''],
      apellidos: [''],
      fechaNacimiento: [''],
      departamento: [''],
    });
  }

  guardar() {
    const emp = localStorage.getItem("empleados")
    if (emp) {
      this.empleados = JSON.parse(emp!)
      const index = this.empleados.findIndex(res => res.dni == this.empleado.dni)
      const existe = this.empleados.find(empleado => empleado.dni == this.empleadoForm.value.dni)
      if (!existe) {
        this.empleados[index] = this.empleado
        localStorage.setItem("empleados", JSON.stringify(this.empleados))
        this.alertaEditado()
      } else {
        this.componentService.alerta("info", "Ya existe el DNI")
      }
    }
    
  }

  alertaEditado(){
    this.componentService.alerta("success", "Se ha editado el empleado").then(res => {
      if (res.isConfirmed) {
        this.dialogRef.close("editado")
      }
    })
  }

}
