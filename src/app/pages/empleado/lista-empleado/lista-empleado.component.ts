import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { CrearEmpleadoComponent } from '../crear-empleado/crear-empleado.component';
import { MatDialog } from '@angular/material/dialog';
import { Empleado } from '../../model/empleado.interface';
import { ComponentesService } from 'src/app/services/componentes.service';
import { EditarEmpleadoComponent } from '../editar-empleado/editar-empleado.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-lista-empleado',
  templateUrl: './lista-empleado.component.html',
  styleUrls: ['./lista-empleado.component.scss'],
})




export class ListaEmpleadoComponent implements OnInit {

  columnas: any[] = ["dni", "nombres", "apellidos", "fechaNacimiento", "email", "departamento", "opciones"]
  dataFondoSource = new MatTableDataSource<any>();
  empleados: any[] = [];
  constructor(              public dialog: MatDialog,
    private componentService: ComponentesService
    ) { }

  ngOnInit(): void {
    this.cargarEmpleados()
  }

  cargarEmpleados(){
    const emp =  localStorage.getItem("empleados")
    this.empleados = JSON.parse(emp!)
    if (emp) {
      this.dataFondoSource.data = this.empleados
      
    }
  }

  crearEmpleado()  {
    const dialogRef = this.dialog.open(CrearEmpleadoComponent, {
      width: '550px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == "creado") {
        this.cargarEmpleados()
      }
    });
  }

  editarEmpleado(empleado: Empleado)  {
    const dialogRef = this.dialog.open(EditarEmpleadoComponent, {
      width: '550px',
      data: empleado
    },
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result == "editado") {
        this.cargarEmpleados()
      }
    });
  }

  eliminarEmpleado(index: number){
    this.componentService.alertaButtons("info", "Estás seguro de eliminar el empleado").then(res=>{
      if (res.isConfirmed) {
        const emp =  localStorage.getItem("empleados")
        this.empleados = JSON.parse(emp!)
        this.empleados.splice(index, 1)
        localStorage.setItem("empleados", JSON.stringify(this.empleados))
        this.cargarEmpleados()
      }
    })
  }

}
