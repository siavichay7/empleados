import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditarEmpleadoComponent } from './editar-empleado/editar-empleado.component';
import { CrearEmpleadoComponent } from './crear-empleado/crear-empleado.component';
import { ListaEmpleadoComponent } from './lista-empleado/lista-empleado.component';
import { EmpleadoRoutingModule } from './empleado-routing.module';
import { ComponentModule } from './component.module';


@NgModule({
  declarations: [
    CrearEmpleadoComponent,
    EditarEmpleadoComponent,
    ListaEmpleadoComponent,
  ],
  imports: [
    CommonModule,
    EmpleadoRoutingModule,
    ComponentModule
  ], exports: [
    ComponentModule
  ]
})
export class EmpleadoModule { }
