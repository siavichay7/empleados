import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrearEmpleadoComponent } from './crear-empleado/crear-empleado.component';
import { EditarEmpleadoComponent } from './editar-empleado/editar-empleado.component';
import { ListaEmpleadoComponent } from './lista-empleado/lista-empleado.component';

const routes: Routes = [
  {path: "empleados", component: ListaEmpleadoComponent},
  {path: '', redirectTo: 'empleados', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class EmpleadoRoutingModule { }
