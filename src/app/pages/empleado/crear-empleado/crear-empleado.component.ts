import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ComponentesService } from 'src/app/services/componentes.service';

@Component({
  selector: 'app-crear-empleado',
  templateUrl: './crear-empleado.component.html',
  styleUrls: ['./crear-empleado.component.scss']
})
export class CrearEmpleadoComponent implements OnInit {

  empleadoForm!: FormGroup;
  departamentos: any[] = ["Administración", "Tienda", "Contabilidad"];
  empleados: any[] = [];

  constructor(private fb: FormBuilder,
    private dialogRef: MatDialogRef<CrearEmpleadoComponent>,
    private componentService: ComponentesService
  ) { }

  ngOnInit(): void {
    this.empleadoForm = this.fb.group({
      dni: ['', Validators.required],
      nombres: ['', Validators.required],
      apellidos: [''],
      email: ['', Validators.required, Validators.email],
      fechaNacimiento: [''],
      departamento: [''],
    });
  }

  guardar() {
    const emp = localStorage.getItem("empleados")
    if (emp) {
      this.empleados = JSON.parse(emp!)
      const existe = this.empleados.find(empleado => empleado.dni == this.empleadoForm.value.dni)
      if (!existe) {
        this.empleados.push(this.empleadoForm.value)
        localStorage.setItem("empleados", JSON.stringify(this.empleados))
        this.alertaCreado()
      } else {
        this.componentService.alerta("info", "Ya existe el DNI")
      }
    } else {
      this.empleados.push(this.empleadoForm.value)
      localStorage.setItem("empleados", JSON.stringify(this.empleados))
      this.alertaCreado()
    }

  }

  alertaCreado() {
    this.componentService.alerta("success", "Se ha creado el empleado").then(res => {
      if (res.isConfirmed) {
        this.dialogRef.close("creado")
      }
    })
  }

}
