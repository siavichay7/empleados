export interface Empleado {
    dni?: string;
    nombres?: string;
    apellidos: string;
    fechaNacimiento: string;
    email: string;
    departamento: string;
}